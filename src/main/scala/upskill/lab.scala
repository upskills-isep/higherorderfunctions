package upskill

import scala.annotation.tailrec

object lab:

  def f(f1: Int => Int, a: Int, b: Int) : Int=

    @scala.annotation.tailrec
    def fAux(x: Int, acc: Int): Int =
      if (x > b) acc
      else fAux(x + 1, acc + f1(x))

    fAux(a,0 )


  def g1(x: Int): Int = ???

  def g2(x: Int): Int = ???

  def g3(x: Int): Int = ???

  def g4(x: Int): Int = ???

  def g5(x: Int): Int = ???

  def fStr(f: String => String, str: String) = f(str)

  def fStr1(str: String) = ???
  def fStr2(str: String) = ???
  def fStr3(str: String) = ???
  def fStr4(str: String) = ???
